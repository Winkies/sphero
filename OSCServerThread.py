#!/usr/bin/env python
# -*- coding:utf-8 -*-

import threading
import time
import OSC
from view import *
from connect import *

class   OSCServerThread(threading.Thread):
    """
    This class implements thread python in order to get
    data from each sphero at the same time
    """
    def __init__(self, obj, scene):
        """
        Initialize Thread class

        """
        threading.Thread.__init__(self)
        #------OSC Server-------------------------------------#
        

	self.receive_address = '192.168.0.101', 9000 #(JeffNetwork)
	#self.receive_address = '127.0.0.1', 9000 #(JeffTib)


        # OSC Server. there are three different types of server. 
        self.s = OSC.ThreadingOSCServer(self.receive_address) #Jeff
        # this registers a 'default' handler (for unmatched messages)
        self.s.addDefaultHandlers()
        self.s.addMsgHandler("/d100", self.updatePath_handler)
        self.s.addMsgHandler("/d101", self.updatePath_handler)
	self.s.addMsgHandler("/d102", self.updatePath_handler)
	self.s.addMsgHandler("/d103", self.updatePath_handler)
	self.s.addMsgHandler("/d104", self.updatePath_handler)
	self.s.addMsgHandler("/d105", self.updatePath_handler)
	self.s.addMsgHandler("/d106", self.updatePath_handler)
	self.s.addMsgHandler("/d107", self.updatePath_handler)
	self.s.addMsgHandler("/d108", self.updatePath_handler)
	self.s.addMsgHandler("/d109", self.updatePath_handler)
	self.s.addMsgHandler("/d110", self.updatePath_handler)
	self.s.addMsgHandler("/d111", self.updatePath_handler)
	self.s.addMsgHandler("/d112", self.updatePath_handler)
	self.s.addMsgHandler("/d113", self.updatePath_handler)
	self.s.addMsgHandler("/d114", self.updatePath_handler)
	self.s.addMsgHandler("/d115", self.updatePath_handler)
	self.s.addMsgHandler("/d116", self.updatePath_handler)
	self.s.addMsgHandler("/d117", self.updatePath_handler)
	self.s.addMsgHandler("/d118", self.updatePath_handler)
	self.s.addMsgHandler("/d119", self.updatePath_handler)
	self.s.addMsgHandler("/d120", self.updatePath_handler)
	self.s.addMsgHandler("/d121", self.updatePath_handler)
	self.s.addMsgHandler("/d122", self.updatePath_handler)
    	self.s.addMsgHandler("/green", self.updateSpheroData_handler)
    	self.s.addMsgHandler("/black", self.updateSpheroData_handler)
    	self.s.addMsgHandler("/blue", self.updateSpheroData_handler)
    	self.s.addMsgHandler("/cyan", self.updateSpheroData_handler)
    	self.s.addMsgHandler("/gray", self.updateSpheroData_handler)
    	self.s.addMsgHandler("/magenta", self.updateSpheroData_handler)
    	self.s.addMsgHandler("/red", self.updateSpheroData_handler)
    	self.s.addMsgHandler("/darkRed", self.updateSpheroData_handler)


        self.scene = scene
        self.path = ''
        self.objet = obj

    def closeServer(self):
        self.s.close()
        

    def run(self):
        """
        This function is called by start thread instruction.
        
        """
        print "Starting"

        self.s.serve_forever()
                                  
    def updatePath_handler(self, addr, tags, msg, source):
        if addr=='/d100':
                self.path='/home/thibaut/Documents/spherofev/chore/d01/d100.xml'
                self.launchSequenceFromXml()
        if addr=='/d101':
                self.path='/home/thibaut/Documents/spherofev/chore/d01/d101.xml'
		self.launchSequenceFromXml()
        if addr=='/d102':
                self.path='/home/thibaut/Documents/spherofev/chore/d01/d102.xml'
		self.launchSequenceFromXml()
        if addr=='/d103':
                self.path='/home/thibaut/Documents/spherofev/chore/d01/d103.xml'
		self.launchSequenceFromXml()
        if addr=='/d104':
                self.path='/home/thibaut/Documents/spherofev/chore/d01/d104.xml'
		self.launchSequenceFromXml()
        if addr=='/d105':
                self.path='/home/thibaut/Documents/spherofev/chore/d01/d105.xml'
		self.launchSequenceFromXml()
        if addr=='/d106':
                self.path='/home/thibaut/Documents/spherofev/chore/d01/d106.xml'
		self.launchSequenceFromXml()
        if addr=='/d107':
                self.path='/home/thibaut/Documents/spherofev/chore/d01/d107.xml'
		self.launchSequenceFromXml()
        if addr=='/d108':
                self.path='/home/thibaut/Documents/spherofev/chore/d01/d108.xml'
		self.launchSequenceFromXml()
        if addr=='/d109':
                self.path='/home/thibaut/Documents/spherofev/chore/d01/d109.xml'
                self.launchSequenceFromXml()
        if addr=='/d110':
                self.path='/home/thibaut/Documents/spherofev/chore/d01/d110.xml'
                self.launchSequenceFromXml()
        if addr=='/d111':
                self.path='/home/thibaut/Documents/spherofev/chore/d01/d111.xml'
                self.launchSequenceFromXml()
        if addr=='/d112':
                self.path='/home/thibaut/Documents/spherofev/chore/d01/d112.xml'
                self.launchSequenceFromXml()
        if addr=='/d113':
                self.path='/home/thibaut/Documents/spherofev/chore/d01/d113.xml'
                self.launchSequenceFromXml()
        if addr=='/d114':
                self.path='/home/thibaut/Documents/spherofev/chore/d01/d114.xml'
                self.launchSequenceFromXml()
        if addr=='/d115':
                self.path='/home/thibaut/Documents/spherofev/chore/d01/d115.xml'
                self.launchSequenceFromXml()
        if addr=='/d116':
                self.path='/home/thibaut/Documents/spherofev/chore/d01/d116.xml'
                self.launchSequenceFromXml()
        if addr=='/d117':
                self.path='/home/thibaut/Documents/spherofev/chore/d01/d117.xml'
                self.launchSequenceFromXml()
        if addr=='/d118':
                self.path='/home/thibaut/Documents/spherofev/chore/d01/d118.xml'
                self.launchSequenceFromXml()
        if addr=='/d119':
                self.path='/home/thibaut/Documents/spherofev/chore/d01/d119.xml'
                self.launchSequenceFromXml()
        if addr=='/d120':
                self.path='/home/thibaut/Documents/spherofev/chore/d01/d120.xml'
                self.launchSequenceFromXml()
        if addr=='/d121':
                self.path='/home/thibaut/Documents/spherofev/chore/d01/d121.xml'
                self.launchSequenceFromXml()
        if addr=='/d122':
                self.path='/home/thibaut/Documents/spherofev/chore/d01/d122.xml'
                self.launchSequenceFromXml()

    def launchSequenceFromXml(self):
        """
        Load the file from QFile Widget and call the launcher
        """
        path = self.path
        self.scene.clearImage()
        tmp = Save(self.objet)
        tmp.readXmlToScene(path)
        self.scene.drawFromFile()
        self.scene.threadRoll()



    # define a message-handler function for the server to call.
    def printing_handler(self, addr, tags, stuff, source):
            if addr=='/green':
                    print "Adress = green", stuff
	            self.threadRoll("green")
            elif addr=='/black':
                    print "Adress = black", stuff
	            self.threadRoll("black")
            elif addr=='/red':
                    print "Adress = red", stuff
		    self.threadRoll("red")
            elif addr=='/darkRed':
                    print "Adress = darkRed", stuff
		    self.threadRoll("darkRed")
            elif addr=='/gray':
                    print "Adress = gray", stuff
		    self.threadRoll("gray")
            elif addr=='/cyan':
                    print "Adress = cyan", stuff
		    self.threadRoll("cyan")
            elif addr=='/magenta':
                    print "Adress = magenta", stuff
                    self.threadRoll("magenta")
            elif addr=='/blue':
                    print "Adress = blue", stuff
                    self.threadRoll("blue")

    def updateSpheroData_handler(self, addr, tags, msg, source):
            "update speed and positions of the spheros"

            if addr=='/green':
                    spheroItem = self.objet.returnObject("green")
                    spheroItem.point.setScene(msg[0], msg[1])
                    print "width of the scene in meters : ", spheroItem.point.widthMeter
                    print "height of the scene in meters : ", spheroItem.point.heightMeter
                    spheroItem.point.addSpeedInList(msg[2])
                    print spheroItem.point.list_speed
                    spheroItem.point.addPointInMeterInList(msg[3], msg[4])
                    print spheroItem.point.list_point_meter

            elif addr=='/black':
                    spheroItem = self.objet.returnObject("black")
                    spheroItem.point.setScene(msg[0], msg[1])
                    print "width of the scene in meters : ", spheroItem.point.widthMeter
                    print "height of the scene in meters : ", spheroItem.point.heightMeter
                    spheroItem.point.addSpeedInList(msg[2])
                    print spheroItem.point.list_speed
                    spheroItem.point.addPointInMeterInList(msg[3], msg[4])
                    print spheroItem.point.list_point_meter

            elif addr=='/blue':
                    spheroItem = self.objet.returnObject("blue")
                    spheroItem.point.setScene(msg[0], msg[1])
                    print "width of the scene in meters : ", spheroItem.point.widthMeter
                    print "height of the scene in meters : ", spheroItem.point.heightMeter
                    spheroItem.point.addSpeedInList(msg[2])
                    print spheroItem.point.list_speed
                    spheroItem.point.addPointInMeterInList(msg[3], msg[4])
                    print spheroItem.point.list_point_meter

            elif addr=='/cyan':
                    spheroItem = self.objet.returnObject("cyan")
                    spheroItem.point.setScene(msg[0], msg[1])
                    print "width of the scene in meters : ", spheroItem.point.widthMeter
                    print "height of the scene in meters : ", spheroItem.point.heightMeter
                    spheroItem.point.addSpeedInList(msg[2])
                    print spheroItem.point.list_speed
                    spheroItem.point.addPointInMeterInList(msg[3], msg[4])
                    print spheroItem.point.list_point_meter

            elif addr=='/red':
                    spheroItem = self.objet.returnObject("red")
                    spheroItem.point.setScene(msg[0], msg[1])
                    print "width of the scene in meters : ", spheroItem.point.widthMeter
                    print "height of the scene in meters : ", spheroItem.point.heightMeter
                    spheroItem.point.addSpeedInList(msg[2])
                    print spheroItem.point.list_speed
                    spheroItem.point.addPointInMeterInList(msg[3], msg[4])
                    print spheroItem.point.list_point_meter

            elif addr=='/darkRed':
                    spheroItem = self.objet.returnObject("darkRed")
                    spheroItem.point.setScene(msg[0], msg[1])
                    print "width of the scene in meters : ", spheroItem.point.widthMeter
                    print "height of the scene in meters : ", spheroItem.point.heightMeter
                    spheroItem.point.addSpeedInList(msg[2])
                    print spheroItem.point.list_speed
                    spheroItem.point.addPointInMeterInList(msg[3], msg[4])
                    print spheroItem.point.list_point_meter
        

            elif addr=='/gray':
                    spheroItem = self.objet.returnObject("gray")
                    spheroItem.point.setScene(msg[0], msg[1])
                    print "width of the scene in meters : ", spheroItem.point.widthMeter
                    print "height of the scene in meters : ", spheroItem.point.heightMeter
                    spheroItem.point.addSpeedInList(msg[2])
                    print spheroItem.point.list_speed
                    spheroItem.point.addPointInMeterInList(msg[3], msg[4])
                    print spheroItem.point.list_point_meter

            elif addr=='/magenta':
                    spheroItem = self.objet.returnObject("magenta")
                    spheroItem.point.setScene(msg[0], msg[1])
                    print "width of the scene in meters : ", spheroItem.point.widthMeter
                    print "height of the scene in meters : ", spheroItem.point.heightMeter
                    spheroItem.point.addSpeedInList(msg[2])
                    print spheroItem.point.list_speed
                    spheroItem.point.addPointInMeterInList(msg[3], msg[4])
                    print spheroItem.point.list_point_meter

    def threadRoll(self):	
        """
        Create Thread Object corresponding to the sphero we want to communicate with
        """
        threads = []
        mutex = []
        list_sphero = self.objet.returnListObjet()
        for item in list_sphero:
                if (item.connect == True):
                        print "Name sphero avant le toThread "
                        print item.name
                        print item.color
                        print item.addr
                        print item.socket
                        print "\end avant toThread /"
                        t = toThread(item)
                        t.start()
                        threads.append(t)

        for t in threads:
            t.join()


    def autoConnect(self):
            "This method is copied from window.py"
            "The Connect object normally lauch autoSave"
            "when you click on connect"
            "autoSave creates the tuple of Sphero objects that"
            "is returned by getSave"
            
            
            c = Connect()
            c.autoConnect()
            c.autoSave()
            i = c.getSave()
            if (i):
                self.setObjet(i)

    def setObjet(self, obj):
            """
            Set infos for Sphero object when device is connected

            :param obj: tuple return by Connect object contains name, addr, and socket
            :type obj: tuple(string, string, string, SpheroAPI, bool)
            """
            for tmp in obj:
                    if (tmp[0] == "Sphero-RBR"):
                            the_object = self.objet.returnObject("black")
                            the_object.setInfos(tmp)
                    elif (tmp[0] == "Sphero-GRY"):
                            the_object = self.objet.returnObject("blue")
                            the_object.setInfos(tmp)
                    elif (tmp[0] == "Sphero-AAA"):
                            the_object = self.objet.returnObject("green")
                            the_object.setInfos(tmp)
                    elif (tmp[0] == "Sphero-GRY"):
                            the_object = self.objet.returnObject("darkRed")
                            the_object.setInfos(tmp)
                    elif (tmp[0] == "Sphero-BYO"):
                            the_object = self.objet.returnObject("cyan")
                            the_object.setInfos(tmp)
                    elif (tmp[0] == "Sphero-GGB"):
                            the_object = self.objet.returnObject("gray")
                            the_object.setInfos(tmp)
                    elif (tmp[0] == "Sphero-BOY"):
                            the_object = self.objet.returnObject("red")
                            the_object.setInfos(tmp)
                    else:
                            pass


    def sendData(self, spheroItem):
            spheroItem.point.addSpeedInList(self.listSpeeds)
            spheroItem.point.addPointInList(pos)



