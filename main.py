#/usr/bin/python
# -*- coding:utf-8 -*-

"""
Main file
"""

import sys
from PyQt4.QtGui import *
from PyQt4.QtCore import *
from window import *

def     main():
    """
    call MainWindow object
    """
    # av = 'file.cfg'
    # sys.argv = ['file.cfg']
    av = None
    if (len(sys.argv) < 2):
        print "[Missing configuration file]"
    elif len(sys.argv) == 2 and os.path.isfile(sys.argv[1]):
       av = sys.argv[1]
    else:
      print >> sys.stderr, "Usage: " + sys.argv[0] + "file.conf"
      sys.exit(1)
    app = QApplication(sys.argv)
    mainWd = MainWindow(av)
    mainWd.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()

