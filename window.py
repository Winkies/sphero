#!/usr/bin/python
# -*- coding:utf-8 -*-

"""
Init MainWindow class
"""
from PyQt4.QtGui import *
from PyQt4.QtCore import *
from view import *
from layout import *
from save import *
from connect import *
from objet import *
from OSCServerThread import *
from OSCSend import *
from OSCGetSpheroDataThread import *
import bluetooth

class   MainWindow(QMainWindow):
    """
    Create and manage main window which one user can see
    when execute the command line: python main.py
    """
    def __init__(self, av):
        super(MainWindow, self).__init__()

        self.obj = Objet()
        self.saveAsActs = []
        self.path = ''
        self.fillColorToolButton = None

        self.scene = View(self, self.obj, av)
        self.setCentralWidget(self.initLayout())
        self.initWindow()
        self.createActions()
        self.createMenus()

        #Start Connexion and OSCServer
        
        #Comment mael
        #self.OSCServerThread = OSCServerThread(self.obj, self.scene)
        
        #self.OSCServerThread.autoConnect()
        
        #Comment mael
        #self.OSCServerThread.start()
        
        #self.OSCServerThread.closeServer()       
        #self.OSCServerThread.threadRoll() "envoie les données. Gerer a l interieur de OSCServerThread ?"

        #Start OSCClient
 	#self.OSCClient = OSCGetSpheroDataThread(self.obj)
 	#self.OSCClient.start()      

	#Start OSCClient
    #Comment mael
	#self.OSCClient = OSCSend(self.obj)
	#self.OSCClient.sendSpheroData()   
        

    def initWindow(self):
        """
        Full screen
        Set resize with Minimum size
        Set name of window
        """
        self.showMaximized()
        self.setMinimumSize(500, 500)
        self.setWindowTitle("Sphero GUI")

    def keyPressEvent(self, event):
        """
        Implements keyPressEvent
        Catch escape touch and close windows

        :param event: touch pressed
        :type event: QKeyEvent
        """
        key = event.key()

        if (key == Qt.Key_Escape):
            self.close()
        else:
            super(MainWindow, self).keyPressEvent(event)

    def initLayout(self):
        """
        Init Layout: place QPushButton, QImage and QListWidget
        with layout.py
        """
        frame = QFrame(self)
        layout = BorderLayout()
        gridlayout = QGridLayout()
        vlayout = QVBoxLayout()

        self.scene.clearImage()
        self.scene.mainWindow = self
        vlayout.addWidget(self.scene.load_widget)
        vlayout.addWidget(self.scene.load_file_widget)
        gridlayout.addWidget(self.scene.file_widget.treeView, 1, 0)
        gridlayout.addLayout(vlayout, 1, 1)
        #gridlayout.addWidget(self.scene.load_file_widget, 2, 1)
        layout.add(gridlayout, BorderLayout.North)
        layout.addWidget(self.scene, BorderLayout.Center)
        layout.addWidget(self.scene.listspeed, BorderLayout.East)
        layout.addWidget(self.scene.listview, BorderLayout.East)
        frame.setLayout(layout)
        return (frame)

    def penColor(self):
        """
        Color pen tab
        """
        newColor = QtGui.QColorDialog.getColor(self.scene.penColor())
        if newColor.isValid():
            self.scene.setPenColor(newColor)

    def penWidth(self):
        """
        Width pen tab
        """
        newWidth, ok = QtGui.QInputDialog.getInteger(self, "Sphero Gui",
            "Select pen width:", self.scene.penWidth(), 1, 50, 1)
        if ok:
            self.scene.setPenWidth(newWidth)

    def about(self):
        """
        About tab
        """
        QtGui.QMessageBox.about(self, "About SpheroGUI",
            "<p>The <b>SpheroGUI</b> is interface for "
            "ballet dancing with sphero toy. Enjoy !</p>")

    def createActions(self):
        """
        QAction for Open (Ctrl+o), Save(Ctrl+s), Exit(Ctrl+q) tab
        """
        self.openAct = QtGui.QAction("&Open...", self, shortcut="Ctrl+O",
            triggered=self.chooseFile)

        self.saveAsMenu = QtGui.QAction("&Save..", self, shortcut="Ctrl+S",
                                        triggered=self.save)

        self.exitAct = QtGui.QAction("&Exit", self, shortcut="Ctrl+Q",
            triggered=self.close)

        self.penColorAct = QtGui.QAction("&Pen Color...", self,
            triggered=self.penColor)

        self.penWidthAct = QtGui.QAction("Pen &Width...", self,
            triggered=self.penWidth)

        self.clearScreenAct = QtGui.QAction("&Clear Screen", self,
            shortcut="Ctrl+L", triggered=self.scene.clearImage)

        self.aboutAct = QtGui.QAction("&About", self, triggered=self.about)

        self.aboutQtAct = QtGui.QAction("About &Qt", self,
            triggered=QtGui.qApp.aboutQt)

    def createMenus(self):
        fileMenu = QtGui.QMenu("&File", self)
        fileMenu.addAction(self.openAct)
        fileMenu.addAction(self.saveAsMenu)
        fileMenu.addSeparator()
        fileMenu.addAction(self.exitAct)

        optionMenu = QtGui.QMenu("&Options", self)
        optionMenu.addAction(self.penColorAct)
        optionMenu.addAction(self.penWidthAct)
        optionMenu.addSeparator()
        optionMenu.addAction(self.clearScreenAct)

        helpMenu = QtGui.QMenu("&Help", self)
        helpMenu.addAction(self.aboutAct)
        helpMenu.addAction(self.aboutQtAct)

        self.menuBar().addMenu(fileMenu)
        self.menuBar().addMenu(optionMenu)
        self.menuBar().addMenu(helpMenu)


        self.fillColorToolButton = QtGui.QToolButton()
        self.setColorToolButton(self.fillColorToolButton)
        self.fillColorToolButton.setPopupMode(QtGui.QToolButton.MenuButtonPopup)
        self.fillColorToolButton.setMenu(
            self.createColorMenu(self.itemColorChanged, QtCore.Qt.white))
        self.fillAction = self.fillColorToolButton.menu().defaultAction()
        self.fillColorToolButton.setIcon(
            self.createColorToolButtonIcon(':/images/floodfill.png',
                                                 QtCore.Qt.black))
        self.fillColorToolButton.clicked.connect(self.scene.setPenColor)

        self.colorToolBar = self.addToolBar("Color")
        self.colorToolBar.addWidget(self.fillColorToolButton)

        self.connexion = QtGui.QPushButton('Connexion', self)
        self.connexion.clicked.connect(self.autoConnect)
        self.connexionToolBar = self.addToolBar("Connexion")
        self.connexionToolBar.addWidget(self.connexion)

        self.led = QtGui.QPushButton('BackLed', self)
        self.led.clicked.connect(self.scene.led)
        self.ledToolBar = self.addToolBar("BackLed")
        self.ledToolBar.addWidget(self.led)

        self.clear = QtGui.QPushButton('Clear', self)
        self.clear.clicked.connect(self.scene.clearImage)
        self.clearToolBar = self.addToolBar("Clear")
        self.clearToolBar.addWidget(self.clear)

        #self.pause = QtGui.QPushButton('Break', self)
        #self.pause.clicked.connect(self.scene.pointBreak)
        #self.pauseToolBar = self.addToolBar("Break")
        #self.pauseToolBar.addWidget(self.pause)

        self.roll = QtGui.QPushButton('Roll', self)
        self.roll.clicked.connect(self.scene.threadRoll)
        self.rollToolBar = self.addToolBar("Roll")
        self.rollToolBar.addWidget(self.roll)

        self.gyro = QtGui.QPushButton('Gyro', self)
        self.gyro.clicked.connect(self.scene.setGyro)
        self.rollToolBar = self.addToolBar("Gyro")
        self.rollToolBar.addWidget(self.gyro)

        self.modif_scene = QtGui.QPushButton('Scene', self)
        self.modif_scene.clicked.connect(self.scene.modify_scene)
        self.rollToolBar = self.addToolBar("Scene")
        self.rollToolBar.addWidget(self.modif_scene)

    def save(self):
        tmp = self.scene.saveXml()
        """ Provides a dialog window to allow the user to save the image file.
        """
        imageFile = QtGui.QFileDialog.getSaveFileName(self,
                                                      "Choose a filename to save the image", "", "Images (*.xml)")

        info = QtCore.QFileInfo(imageFile)
        newImageFile = ""

        if info.baseName() != '':
            newImageFile = QtCore.QFileInfo(info.absoluteDir(),
                    info.baseName() + '.xml').absoluteFilePath()

        else:
            QtGui.QMessageBox.warning(self, "Cannot save file",
                    "Please enter a valid filename.", QtGui.QMessageBox.Cancel,
                    QtGui.QMessageBox.NoButton, QtGui.QMessageBox.NoButton)

        tmp.saveSceneToXml(newImageFile)


    def chooseFile(self):
        """ Provides a dialog window to allow the user to specify an image file.
        If a file is selected, the appropriate function is called to process
        and display it.
        """
        imageFile = QtGui.QFileDialog.getOpenFileName(self,
                    "Choose an image file to open", self.path, "Images (*.xml)")

        if imageFile != '':
            tmp = self.scene.readXml(imageFile)
            self.path = imageFile

    """
    Create menu bar for color
    """
    def createColorMenu(self, slot, defaultColor):
        colors = [QtCore.Qt.black, QtCore.Qt.blue, QtCore.Qt.green, QtCore.Qt.cyan, QtCore.Qt.gray, QtCore.Qt.yellow, QtCore.Qt.magenta, QtCore.Qt.red, QtCore.Qt.darkRed]
        names = ["black", "blue", "green", "cyan", "gray", "yellow", "magenta", "red", "darkRed"]

        colorMenu = QtGui.QMenu(self)
        for color, name in zip(colors, names):
            action = QtGui.QAction(self.createColorIcon(color), name, self,
                    triggered=slot)
            action.setData(QtGui.QColor(color))
            colorMenu.addAction(action)
            if color == defaultColor:
                colorMenu.setDefaultAction(action)
        return colorMenu


    def createColorToolButtonIcon(self, imageFile, color):
        pixmap = QtGui.QPixmap(50, 80)
        pixmap.fill(QtCore.Qt.transparent)
        painter = QtGui.QPainter(pixmap)
        image = QtGui.QPixmap(imageFile)
        target = QtCore.QRect(0, 0, 50, 60)
        source = QtCore.QRect(0, 0, 42, 42)
        painter.fillRect(QtCore.QRect(0, 60, 50, 80), color)
        painter.drawPixmap(target, image, source)
        painter.end()

        return QtGui.QIcon(pixmap)

    def createColorIcon(self, color):
        pixmap = QtGui.QPixmap(20, 20)
        painter = QtGui.QPainter(pixmap)
        painter.setPen(QtCore.Qt.NoPen)
        painter.fillRect(QtCore.QRect(0, 0, 20, 20), color)
        painter.end()

        return QtGui.QIcon(pixmap)


    def fillButtonTriggered(self):
        self.setItemColor(QtGui.QColor(self.fillAction.data()))

    def itemColorChanged(self):
        self.fillAction = self.sender()
        self.fillColorToolButton.setIcon(self.createColorToolButtonIcon(
            ':/images/floodfill.png',
            QtGui.QColor(self.fillAction.data())))
        self.fillButtonTriggered()

    def setItemColor(self, color):
        self.scene.setPenColor(color)

    def isItemChange(self, type):
        for item in self.selectedItems():
            if isinstance(item, type):
                return True
        return False

    def setColorToolButton(self, tmp):
        """
        setter ColorToolButton
        """
        self.fillColorToolButton = tmp

    """
    End menu bar color
    """

    def autoConnect(self):
        """
        Function called by "Connexion" QPushButton
        Create Connect object so conjure QDialog (new window)
        """
        c = Connect()
        c.autoConnect()
        c.show()
        c.exec_()
        i = c.getSave()

        if (i):
            self.scene.setObjet(i)
