#!/usr/bin/env python
# -*- coding:utf-8 -*-

import threading
import time
import OSC
from view import *
from connect import *

class   OSCTestServerThread(threading.Thread):
    """
    This class implements thread python in order to get
    data from each sphero at the same time
    """
    def __init__(self):
        """
        Initialize Thread class

        """
        threading.Thread.__init__(self)
        #------OSC Server-------------------------------------#
        self.receive_address = '127.0.0.1', 9001
        # OSC Server. there are three different types of server. 
        self.s = OSC.ThreadingOSCServer(self.receive_address)
        # this registers a 'default' handler (for unmatched messages)
        self.s.addDefaultHandlers()
        self.s.addMsgHandler("/yellow", self.printing_handler)
    	
        self.path = ''

    def closeServer(self):
        self.s.close()
        

    def run(self):
        """
        This function is called by start thread instruction.
        
        """
        print "Starting"

        self.s.serve_forever()
                                  
    

    



    # define a message-handler function for the server to call.
    #JEff 192
    def printing_handler(self, addr, tags, stuff, source):
            if addr=='/yellow':
                    print "Adress = yellow", stuff
                    print(addr, " ", stuff)
            elif addr=='/green':
                    print "Adress = green", stuff

    



    def autoConnect(self):
            "This method is copied from window.py"
            "The Connect object normally lauch autoSave"
            "when you click on connect"
            "autoSave creates the tuple of Sphero objects that"
            "is returned by getSave"
            
            
            c = Connect()
            c.autoConnect()
            c.autoSave()
            i = c.getSave()
            if (i):
                self.setObjet(i)

    def setObjet(self, obj):
            """
            Set infos for Sphero object when device is connected

            :param obj: tuple return by Connect object contains name, addr, and socket
            :type obj: tuple(string, string, string, SpheroAPI, bool)
            """
            for tmp in obj:
                    if (tmp[0] == "Sphero-RBR"):
                            the_object = self.objet.returnObject("black")
                            the_object.setInfos(tmp)
                    elif (tmp[0] == "Sphero-GRY"):
                            the_object = self.objet.returnObject("blue")
                            the_object.setInfos(tmp)
                    elif (tmp[0] == "Sphero-GRY"):
                            the_object = self.objet.returnObject("green")
                            the_object.setInfos(tmp)
                    elif (tmp[0] == "Sphero-GYW"):
                            the_object = self.objet.returnObject("yellow")
                            the_object.setInfos(tmp)
                    elif (tmp[0] == "Sphero-GGB"):
                            the_object = self.objet.returnObject("gray")
                            the_object.setInfos(tmp)
                    elif (tmp[0] == "Sphero-GRY"):
                            the_object = self.objet.returnObject("darkRed")
                            the_object.setInfos(tmp)
                    elif (tmp[0] == "Sphero-BOY"):
                            the_object = self.objet.returnObject("red")
                            the_object.setInfos(tmp)
                    elif (tmp[0] == "Sphero-BYO"):
                            the_object = self.objet.returnObject("cyan")
                            the_object.setInfos(tmp)

                    else:
                            pass


    def sendData(self, spheroItem):
            spheroItem.point.addSpeedInList(self.listSpeeds)
            spheroItem.point.addPointInList(pos)



