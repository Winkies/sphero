MakerSphere

Introduction:

In the context of contemporary dance choreographer collective Eukaryota asked
to Epitech students to achieve spheres of twenty centimeter diameter robotics and
controllables with Bluetooth using an ergonomic interface.

The project is divided in two parts : Hardware and Software.

For the hardware, spheres based on Sphero (rbr.gosphero.com/) and 3D Printing.

Requirements:
 Software:
	- Python 2.7
	- PyQt-4.10
	- Pyqwt-5.2.0
	- Numpy
	- PyBluez

 Hardware:
	- Bluetooth dongle Class 1, Bluetooth 2.0



Features:
 - Connect to many spheros (GRY/GRY/GRY)
 - Associate color drawing to device connected
 - Set back led of spheros
 - Drawing trajectory (linear), set speed for each line
 - Save choregraphy in file (xml format)
 - Reload saved file

TODO:
 - Replace QImage by one object implementing QImage (for use QPainter without event)
 - Set Gyroscope variable in Sphero object with QPushButton
 - Set real scene size variable in View object and Point object
 - Add break in Rolling function
 - Create widget to list files in current directory with QListWidget and QPushbutton to
   "launch" one or many xml file
 - Replace QListView for items and speeds by QAbstractItemModel
 - Replace the current layout by one homemade or use QtDesigner to place Widget
 - View OpenCV


Usage:
 python main.py

Note:
 The control system is based on SpheroNAV created by Nistad, Simon Andreas Engstrøm
 and Faulkner.
 The original code can be found at the following site:
    http://munin.uit.no/handle/10037/6545?show=full&locale-attribute=en
    https://github.com/faulkner/sphero

