#!/usr/bin/env python
# -*- coding:utf-8 -*-

"""
Third Windows to set Led's Sphero
"""

from PyQt4.QtGui import *
from PyQt4.QtCore import *
import streaming
from error import SpheroError, SpheroConnectionError,\
 SpheroFatalError, SpheroRequestError
from constants import MotorMode
from core import SpheroAPI
import bluetooth
import time

class   Gyro(QDialog):
    """
    This windows allows to set the gyroscopic paramater
    for each device connected
    """
    def __init__(self, obj):
        """
        This class implements QDialog class.
        One Combo Box lists deviced connected and
        QPushButton allows to set the gyroscopic paramater.

        :param obj: list of deviced connected
        :type obj: Sphero Object list
        """
        QDialog.__init__(self)

        self.obj = obj
        self.currentGyro = None
        self.combo = QComboBox()

        self.tmp = []

        self.initCombo()
        self.initLed()
        self.initLayout()

    def initLed(self):
        """
        Init size and title of window
        """
        self.setGeometry(500, 500, 500, 500)
        self.setWindowTitle("Gyro Windows")


    def initLayout(self):
        """
        Init Layout of window: QComboBox, QPushButton
        """
        layout = QVBoxLayout(self)

        self.combo.addItems(self.tmp)
        self.combo.activated[str].connect(self.onActivated)
        ok = QPushButton("OK")
        ok.clicked.connect(self.ok)
        gyro = QPushButton("Set Gyro")
        gyro.clicked.connect(self.setGyro)
        layout.addWidget(self.combo)
        layout.addWidget(gyro)
        layout.addWidget(ok)

    def initCombo(self):
        """
        Set QComboBox with name of all sphero
        """
        a = 0
        for i in self.obj:
            if i.connect == True:
                if (a == 0):
                    self.currentGyro = i
                self.tmp.append(i.name)
                a += 1

    def keyPressEvent(self, event):
        """
        Function event allows to exit with escape key

        :param event: touch pressed
        :type event: QKeyEvent
        """
        key = event.key()

        if (key == Qt.Key_Escape):
            self.close()
        else:
            super(Led, self).keyPressEvent(event)

    def onActivated(self, text):
        """
        Function event to QComboBox to set self.currentLed
        self.currentLed defined which sphero must be rotate

        :param text: selected device name
        :type text: string
        """
        for i in self.obj:
            if i.name == text:
                self.currentGyro = i

    def ok(self):
        """
        Close window and back Led when "ok" button is clicked
        """
        for i in self.obj:
            if (i.connect == True):
                #i.socket.set_back_led_output(0)
		i.socket.configure_locator(0, 0, yaw_tare=90, auto=False) #JEff 
        self.close()



    def setGyro(self):
        """
        Modify the gyroscopic paramater from list of Sphero Object
        """
        f, ok = QInputDialog.getDouble(self, "Gyro", "Set gyro param:", 0.0, -10000, 10000, 2)
        if ok:
            self.currentGyro.setGyro(f)
