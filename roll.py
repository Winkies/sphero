#/usr/bin/env python
# -*- coding:utf-8 -*-

"""
Init Roll object
"""

import math
import streaming
import time
from error import SpheroError, SpheroConnectionError, SpheroFatalError, SpheroRequestError
from constants import MotorMode
from core import SpheroAPI
from PyQt4 import QtCore, QtGui
from point import *

class   Roll(object):
    """
    This class implements angle and distance calculation in order to
    send instruction roll to sphere.
    It used gyroscope to know position sphere and calculate the distance
    to go in scene.
    """
    def Rolling(self, data_objet):
        """
        This functions is the main algorithm to move the sphero.
        It called by toThread Object.
        It calculates the difference between the current position
        returned by gyroscope and the target position.

        :param data_objet: informations (socket, name, paramater gyroscopic, ..)
        of one device
        :type data_objet: Sphero Object
        """
        s = data_objet.socket
        gyro = data_objet.param_gyro
        name = data_objet.name
        speed = data_objet.point.list_speed
        list_point = data_objet.point.list_point_meter
        list_pixel = data_objet.point.list_point_pixel
        i = 0
        while i+1 < len(list_point):
            posx = s.read_locator().x_pos
            posy = s.read_locator().y_pos
            posgyro = QtCore.QPointF(posx, posy)
            posimage = self.gyroimage_function(posgyro, gyro)
            angle = 0
            diff_theoric = self.diff(list_point[i], list_point[i+1])
            angle_theoric = self.cal_angle(diff_theoric[0], diff_theoric[1])

            i += 1
            sp = int(speed[i].text())

            a = 0
            pointimage = QtCore.QPointF(list_point[i].x(), list_point[i].y())
            dist_theoric = self.dist(posimage, pointimage)

            dist = -1
            pb = math.sqrt(100 - sp) / 10
            while (dist > 160*(sp+20)/100 or dist == -1): #tant que la distance est superieur à si sp=80 et disttheo=200
                diff = self.diff(posimage, pointimage)
                angle = self.cal_angle(diff[0], diff[1])
                if (a < 3):
                    posx = s.read_locator().x_pos
                    posy = s.read_locator().y_pos
                    posgyro = QtCore.QPointF(posx, posy)
                    posimage = self.gyroimage_function(posgyro, gyro)
                    dist = self.dist(posimage, pointimage)
                    s.roll(sp, int(angle_theoric))
                    time.sleep(0.15)

                if (a >= 3):
                    posx = s.read_locator().x_pos
                    posy = s.read_locator().y_pos
                    posgyro = QtCore.QPointF(posx, posy)
                    posimage = self.gyroimage_function(posgyro, gyro)
                    dist = self.dist(posimage, pointimage)
                    s.roll(sp, int(angle))
                    time.sleep(0.2)
                a += 1
            sp = 40
	    #pb2 = math.pow((sp), 1/3) / 4 # / 4.3 initaillement       
	    #while (dist > math.pow(math.pow(dist_theoric,1) * math.pow((1 - pb2),1),1) ): #dist > .3 * distance initiale avec .3 = (1-pb2)
 	    # A vitesse de 40 pour le JEU DE GO & distth * .3 encore trop court 
            # distb2 = dist (premiere rentree dans la boucle 2 plus precise en fonction du ration dist/dist_theorique)
            #while (dist > dist_theoric * .3 ): #dist > .3 * distance initiale avec .3 = (1-pb2)
            #while (dist > (dist_theoric - (dist_theoric * pb2))):	    		
            while (dist > 120): #35cm
                diff = self.diff(posimage, pointimage)
                angle = self.cal_angle(diff[0], diff[1])
                posimage = self.gyroimage_function(posgyro, gyro)
                dist = self.dist(posimage, pointimage)
                s.roll(sp, int(angle))
                time.sleep(0.2)
                posx = s.read_locator().x_pos
                posy = s.read_locator().y_pos
                posgyro = QtCore.QPointF(posx, posy)
		print('distance')
		print(dist)
            sp = 34
            while (dist > 45):	    		
            #while (dist > (1- pb2)): #35cm
		print('boucle Thibaut')
                diff = self.diff(posimage, pointimage)
                angle = self.cal_angle(diff[0], diff[1])
                posimage = self.gyroimage_function(posgyro, gyro)
                dist = self.dist(posimage, pointimage)
                s.roll(sp, int(angle))
                time.sleep(0.2)
                posx = s.read_locator().x_pos
                posy = s.read_locator().y_pos
                posgyro = QtCore.QPointF(posx, posy)
            s.roll(0, int(angle))
            time.sleep(1.2)
        posx = s.read_locator().x_pos
        posy = s.read_locator().y_pos
        posgyro = QtCore.QPointF(posx, posy)
        posimage = self.gyroimage_function(posgyro, gyro)
        tmp = data_objet.point.convertMeterToPixel(posimage)

        data_objet.point.last_gyro = posgyro
        data_objet.point.last_gyro_meter = QtCore.QRectF(tmp.x(), tmp.y(), 4, 4)

        s.roll(0, 0)
        time.sleep(1)

    def gyroimage_function(self, gyroimage, gyro):
        """
        Convert image's position to gyroscope's position

        :param gyroimage: gyrometer position (x, y)
        :type gyroimage: QPointF
        :param gyro: parameter gyroscopic
        :type gyro: float
        :return: converted point
        :rtype:QPointF
        """
        x = gyroimage.y() * gyro
        y = gyroimage.x() * -gyro
        return (QtCore.QPointF(x, y))

    def gyrometer_function(self, gyrometer, gyro):
        """
        Convert gyroscope position to image's gyroscope

        :param gyrometer: gyrometer position (x, y)
        :type gryometer: QPointF
        :param gyro: parameter gyroscopic
        :type gyro: float
        :return: converted point
        :rtype:QPointF
        """
        x = gyrometer.y() / -gyro
        y = gyrometer.x() / gyro

        return (QtCore.QPointF(x, y))

    def dist(self, p0, p1):
        """
        Calculate distance between two points

        :param p0: starting point
        :type p0: QtCore.QPointF
        :param p1: ending point
        :type p1: QtCore.QPointF
        :return: distance between these two points
        :rtype: float
        """
        x = p1.x() - p0.x()
        y = p1.y() - p0.y()
        return (math.sqrt((math.pow(x, 2) + math.pow(y, 2))))

    def diff(self, p0, p1):
        """
        Calculate difference in x and y between two points

        :param p0: starting point
        :type param: QtCore.QPointF
        :param p1: ending point
        :type param: QtCore.QPointF
        :return: difference in x and in y
        :rtype: tuple(x, y)
        """
        return (p1.x() - p0.x(), p1.y() - p0.y())

    def cal_angle(self, x, y):
        """
        Calculate angle with x and y
        If error occurs, return -2

        :param x: difference in x previously calculated in x with diff function
        :type x: float
        :param y: difference in y previously calculated in y with diff function
        :type y: float
        :return: angle in degrees
        :rtype: float
        """
        if (x == 0 and y < 0):
            return (180)
        elif (x == 0 and y > 0):
            return (0)
        elif (x >= 0 and y >= 0) :
            print math.degrees(math.atan(y/x))
            return (abs(90 - abs(math.degrees(math.atan(y/x)))))
        elif (x <= 0 and y >= 0) :
            return (270 + (abs(math.degrees(math.atan(y/x)))))
        elif (x <= 0 and y <= 0) :
            return (180 +(90 - abs(math.degrees(math.atan(y/x)))))
        elif (x >= 0 and y <= 0) :
            return (90 + abs(math.degrees(math.atan(y/x))))
        return -2
