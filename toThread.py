#!/usr/bin/env python
# -*- coding:utf-8 -*-

import threading
import time
from roll import Roll

class   toThread(threading.Thread):
    """
    This class implements thread python in order to send
    instructions in same time to each sphero
    """
    def __init__(self, point_objet):
        """
        Initialize Thread class
        Initialize Roll class

        :param point_objet: this object contains list speeds, list points, ..
        :type point_objet: list of Objet object
        """
        threading.Thread.__init__(self)
        self.roll = Roll()
        self.data_objet = point_objet

    def run(self):
        """
        This function is called by start thread instruction.
        It called Rolling function of Roll class for send instructions to sphero
        """
        print "Starting"
        print self.data_objet.name
        print self.data_objet.addr
        print self.data_objet.socket
        print "Entre dans la fonction Rolling now"
        self.roll.Rolling(self.data_objet)
