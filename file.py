#!/usr/bin/env python
# -*- coding:utf-8 -*-

from PyQt4 import QtCore, QtGui

class   File(QtGui.QWidget):
    def __init__(self, parent=None):
        super(File, self).__init__(parent)

        self.currentPath = QtCore.QDir.currentPath()

        self.model = QtGui.QFileSystemModel(self)
        self.model.setRootPath(self.currentPath)

        self.indexRoot = self.model.index(self.model.rootPath())

        self.treeView = QtGui.QTreeView(self)
        self.treeView.setModel(self.model)
        self.treeView.setRootIndex(self.indexRoot)
        self.treeView.clicked.connect(self.on_treeView_clicked)

        self.filePath = self.currentPath

    @QtCore.pyqtSlot(QtCore.QModelIndex)
    def on_treeView_clicked(self, index):
        indexItem = self.model.index(index.row(), 0, index.parent())

        self.filePath = self.model.filePath(indexItem)

    def get_file_path(self):
        return self.filePath

    def set_file_path(self, file_path):
        self.filePath = file_path

        model = QtGui.QFileSystemModel(self)
        model.setRootPath(self.filePath)

        index = model.index(model.rootPath())

        self.treeView.setModel(model)
        self.treeView.setRootIndex(index)
