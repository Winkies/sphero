#!/usr/bin/env python
# -*- coding:utf-8 -*-

import os
import os.path
import ConfigParser as configparser

class   Parser(object):
    """
    This class allows to parse the configuration file
    """
    def __init__(self, name_file):
        """
        Initialize Parser class from name of the file parsed

        :param name_file: name of the file
        :type name_file: string

	Augementer le param gyro eduit les distances d avancement de la boule
        """
        self.name = name_file

        self.height = 8.0
        self.width = 8.0

        #self.wpr_sphere = 1.85
        self.gry_sphere = 1.8
        self.rbr_sphere = 1.84
	self.byo_sphere = 1.75
	self.ggb_sphere = 1.75
	self.boy_sphere = 1.62
	self.wpr_sphere = 1.80
	#self.aaa_sphere = 1.80

        #self.gyw_sphere = 1.6


	
#GRY = 1.67
#GRY = 1.35
#RBR = 1.3
#BYO = 1.35
#GGB = 1.6
#RBR = 1.5

        self.link = {}

        if (name_file):
            self.parse_file()

    def parse_file(self):
        """
        This function called to parse the configuration file
        """
        path = os.path.join(os.getcwd(), self.name)

        buff = configparser.ConfigParser()
        buff.readfp(open(path))

        for item in buff.sections():
            if (item == 'Scene'):
                self.height = float(buff.get(item, 'height'))
                self.width = float(buff.get(item, 'width'))
            elif (item == 'Sphere'):
                if float(buff.get(item, 'WPR')) != 0:
                    self.wpr_sphere = float(buff.get(item, 'WPR'))
                if float(buff.get(item, 'GRY')) != 0:
                    self.gry_sphere = float(buff.get(item, 'GRY'))
                if float(buff.get(item, 'RBR')) != 0:
                    self.rbr_sphere = float(buff.get(item, 'RBR'))
                if float(buff.get(item, 'BYO')) != 0:
                    self.byo_sphere = float(buff.get(item, 'BYO'))
                if float(buff.get(item, 'GGB')) != 0:
                    self.ggb_sphere = float(buff.get(item, 'GGB'))
                if float(buff.get(item, 'BOY')) != 0:
                    self.boy_sphere = float(buff.get(item, 'BOY'))
                if float(buff.get(item, 'AAA')) != 0:
                    self.aaa_sphere = float(buff.get(item, 'AAA'))
            elif (item == 'Color'):
                for key, value in buff.items(item):
                    self.link[key] = value
            else:
                pass

        print self.link

    def getHeight(self):
        """

        :return: height of the scene
        :rtype: float
        """
        return self.height

    def getWidth(self):
        """

        :return: width of the scene
        :rtype: float
        """
        return self.width

    def gyro_wpr(self):
        """

        :return: paramater gyroscopic of the Sphero-WPR
        :rtype: float
        """
        return self.wpr_sphere

    def gyro_rbr(self):
        """

        :return: paramater gyroscopic of the Sphero-RBR
        :rtype: float
        """
        return self.rbr_sphere


    def gyro_byo(self):
        """

        :return: paramater gyroscopic of the Sphero-BYO
        :rtype: float
        """
        return self.byo_sphere

    def gyro_boy(self):
        """

        :return: paramater gyroscopic of the Sphero-BOY
        :rtype: float
        """
        return self.boy_sphere

    def gyro_ggb(self):
        """

        :return: paramater gyroscopic of the Sphero-GGB
        :rtype: float
        """
        return self.ggb_sphere

    def gyro_gry(self):
        """

        :return: paramater gyroscopic of the Sphero-GRY
        :rtype: float
        """
        return self.gry_sphere

    def gyro_aaa(self):
        """

        :return: paramater gyroscopic of the Sphero-AAA
        :rtype: float
        """
        return self.aaa_sphere

    def get_link(self):
        """

        :return: list of color associated to sphero
        :rtype: list
        """
        return self.link
