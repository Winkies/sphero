#!/usr/bin/env python
# -*- coding:utf-8 -*-

"""
Init Save Object
"""
from PyQt4.QtGui import *
from PyQt4.QtCore import *
from objet import *

class   Save(object):
    """
    This object allows to save picture in xml file.
    It browse the data, save point and speed.

    It allows to open(read) saved file and put information in Point object.
    """
    def __init__(self, objet):
        """
        Initialize Save object

        :param objet: list of data i.e list speeds, list points, ..
        :type objet: list of Objet object
        """
        self.objet = objet

    def saveSceneToXml(self, path):
        """
        Save information(point and speed) for each sphere
        in xml format to file gave by path

        :param path: path save file
        :type path: string
        """
        xmlStreamWriter = QXmlStreamWriter()
        xmlFile = QFile(path)
        if xmlFile.open(QIODevice.WriteOnly) == False:
            return
        xmlStreamWriter.setDevice(xmlFile)
        xmlStreamWriter.setAutoFormatting(True)
        xmlStreamWriter.writeStartDocument()

        xmlStreamWriter.writeStartElement("SceneData")
        xmlStreamWriter.writeAttribute("version", "v1.0")
        the_list = self.objet.returnListObjet()
        for item in the_list:
            a = 0
            the_list_point = item.point.list_point_pixel
            if the_list_point:
                xmlStreamWriter.writeStartElement("Thread")
                xmlStreamWriter.writeAttribute("color", item.color)
                for point in the_list_point:
                    speed = item.point.list_speed[a].text()
                    xmlStreamWriter.writeStartElement("Items")
                    xmlStreamWriter.writeAttribute("xItems", str(point.x()))
                    xmlStreamWriter.writeAttribute("yItems", str(point.y()))
                    xmlStreamWriter.writeAttribute("Speed", str(speed))
                    xmlStreamWriter.writeEndElement()
                    a += 1
                xmlStreamWriter.writeEndElement()

        xmlStreamWriter.writeEndElement()
        xmlStreamWriter.writeEndElement()

    def readXmlToScene(self, path):
        """
        Read information (point and speed) from xml file

        :param path: path file read file
        :type path: string
        :return: information read
        :rtype: Objet object
        """
        fil = QFile(path)

        fil.open(QFile.ReadOnly | QFile.Text)
        reader = QXmlStreamReader()
        reader.setDevice(fil)
        while not reader.atEnd():
            token = reader.readNext()
            if reader.name() == "Thread" and token == QXmlStreamReader.StartElement:
                attribute_thread = reader.attributes()
                color = attribute_thread.value("color").toString()
                object_color = self.objet.returnObject(color).point

            elif reader.name() == "Items":
                attribute_item = reader.attributes()

                x = attribute_item.value("xItems").toString().toFloat()
                y = attribute_item.value("yItems").toString().toFloat()
                sp = attribute_item.value("Speed").toString()

                if x[1] == True and y[1] == True:
                    point = QPointF(x[0], y[0])

                    if (point.x() == -1 and point.y() == -1):
                        object_color.addBreakInList(point)
                    else:
                        object_color.addPointInListFromFile(point)

                    speed = QListWidgetItem(sp)
                    speed.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEditable |
                                   Qt.ItemIsEnabled)

                    object_color.addSpeedInList(speed)
        return self.objet
