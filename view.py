#!/usr/bin/env python
# -*- coding:utf-8 -*-

"""
Init View class
"""
import math
import Queue
from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import pyqtSlot,SIGNAL,SLOT
from itertools import *
from layout import *
from roll import *
from save import *
from toThread import *
from led import *
from decimal import *
import thread
import core
from point import *
from gyro import *
from file import *
from parse import *

class View(QtGui.QWidget):
    """
    This object allows to draw on area
    and all functiona lities about drawing
    """
    def __init__(self, parent, obj, av):
        """
        This class implements QWidget.
        It represents the main drawing area.
        """
        super(View, self).__init__(parent)

        self.setAttribute(QtCore.Qt.WA_StaticContents)
        self.myPenColor = QtGui.QColor(QtCore.Qt.black)

        self.colorHex = ["#000000", "#0000ff", "#00ff00", "#00ffff", "#a0a0a4", "#ffff00", "#ff00ff", "#800000", "#ff0000"]
        self.colorName = ["black", "blue", "green", "cyan", "gray", "yellow", "magenta", "darkRed", "red"]

        self.myPenWidth = 5
        self.myPenWidthPoint = 1

        self.image = QtGui.QImage()
        #self.image = Image()
        #self.image.drawLineFront()

        self.file_cfg = Parser(av)

        self.heigthMeter = 0
        self.widthMeter = 0

        print self.heigthMeter
        print self.widthMeter

        self.width = 0
        self.heigth = 0

        self.currentColor = "black"

        self.listview = QtGui.QListWidget()
        self.listspeed = QtGui.QListWidget()

        self.file_widget = File()
        self.load_widget = QtGui.QPushButton('Load', self)
        self.load_widget.clicked.connect(self.sequence)
        self.load_file_widget = QtGui.QPushButton('Directory', self)
        self.load_file_widget.clicked.connect(self.load_file)

        self.drawFile = False
        self.objet = obj
        self.currentList = []
        self.listspeed.currentItemChanged.connect(self.itemChanged)
        self.clearImage()
        self.update()
        self.set_infos_from_file()

    def paintEvent(self, event):
        """
        This function is necessary to use painter,
        to have a height and width of image,
        to draw line in back ground
        """
        painter = QtGui.QStylePainter(self)
        painter.drawImage(event.rect(), self.image)
        self.width = self.image.width()
        self.heigth = self.image.height()
        print self.width
        print self.heigth

    def resizeEvent(self, event):
        self.resizeImage(self.image, event.size())
        super(View, self).resizeEvent(event)

    def resizeImage(self, image, newSize):
        if image.size() == newSize:
            return

        newImage = QtGui.QImage(newSize, QtGui.QImage.Format_RGB32)
        newImage.fill(QtGui.qRgb(255, 255, 255))
        painter = QtGui.QPainter(newImage)
        painter.drawImage(QtCore.QPoint(0, 0), image)
        self.image = newImage

    def mousePressEvent(self, event):
        """
        Function use during the first mouse click on area drawing
        Add Item to QListItem (items) and QListItem (speed)
        Set variable self.first to True for first click
        Set variable self._start. This variable is last element
        in list simple point
        """
        nbItem = self.currentList.nbPointMeter
        if (nbItem == 0):
            self.listview.addItem('Thread %s' % self.currentColor)
            self.listspeed.addItem('Speed Thread %s' % self.currentColor)

    def pointBreak(self):
        """
        Add Point Break on QListWidget and point list
        Add new point in positional point list with QPointF(-1, -1)
        May be a little problem when sphero need to be in pause in x times
        and another one must be roll... Need to test it.
        """
        f, ok = QtGui.QInputDialog.getDouble(self, "Break", "Time break:", 0.0, -10000, 10000, 2)
        if ok:
            time = str(f)
            nbItem = self.currentList.nbPointMeter
            if (nbItem == 0):
                self.listview.addItem('Thread %s' % self.currentColor)
                self.listspeed.addItem('Speed Thread %s' % self.currentColor)
                self.countIndex += 1
            item = QtGui.QListWidgetItem(time)
            item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEditable |
                          QtCore.Qt.ItemIsEnabled)
            count = self.returnIndex(nbItem, self.currentColor)
            self.listview.insertItem(count,'\titem %s' % str(nbItem+1))
            self.listspeed.insertItem(count, item)
            self.currentList.addSpeedInList(item)
            point = QtCore.QPointF(-1, -1)
            self.currentList.addBreakInList(point)

    def mouseReleaseEvent(self, event):
        """
        Function use from the second mouse click on area drawing
        Add Item to QListItem (items) and QListItem (speed)
        Get start and end point. Start point equal to fist or last click.
        End point equal to new last click (cause mouseReleaseEvent function)
        Draw linw between this two points.
        """
        pos = event.pos()
        the_list = self.currentList.list_point_pixel
        nbItem = self.currentList.nbPointMeter
        print nbItem
        if (len(the_list) >= 1):
            start = QtCore.QPointF(the_list[-1].x(), the_list[-1].y())
            end = QtCore.QPointF(pos.x(), pos.y())

            self.drawSegment(start, end)

            item = QtGui.QListWidgetItem('50')
            item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEditable |
                          QtCore.Qt.ItemIsEnabled)
        else:
            item = QtGui.QListWidgetItem('')

        count = self.returnIndex(nbItem, self.currentColor)
        self.listview.insertItem(count,'\titem %s' % str(nbItem+1))
        self.listspeed.insertItem(count, item)
        self.currentList.addSpeedInList(item)
        self.currentList.addPointInList(pos)

    def returnIndex(self, nbItem, color):
        """
        Return index of ListView in order to add item after ancient item

        :param nbItem: number of item (= length of point/break in list)
        :type nbItem: integer
        :param color: color
        :type color: string
        """
        i = 0
        index = 0
        while (i < len(self.listview)):
            val = self.listview.item(i).text()
            if (str(val).find(color) != -1):
                index = i + nbItem + 1
            i += 1
        if (index == 0):
            index += 2
        return(index)

    def drawSegment(self, point1, point2):
        """
        Draw Line with 2 points on area drawing

        :param point1: starting point in pixel
        :type point1: QtCore.QPointF
        :param point2: ending point in pixel
        :type point2: QtCore.QPointF
        """
        painter = QtGui.QPainter(self.image)
        painter.setPen(QtGui.QPen(self.myPenColor, self.myPenWidth,
                                  QtCore.Qt.SolidLine, QtCore.Qt.RoundCap, QtCore.Qt.RoundJoin))
        painter.drawLine(point1, point2)
        self.update()

    def drawPoint(self, point, color):
        """
        Draw just one point

        :param point: last point return by gyroscope
        :type point: QtCore.QRectF
        :param color: color of this point
        :type color: string
        """
        painter = QtGui.QPainter(self.image)
        painter.setPen(QtGui.QPen(QtGui.QColor(color), self.myPenWidthPoint,
                                  QtCore.Qt.SolidLine, QtCore.Qt.RoundCap, QtCore.Qt.RoundJoin))
        painter.drawEllipse(point)
        self.update()

    def print_(self):
        """
        Need this function cause this class implement QImage object
        """
        printer = QtGui.QPrinter(QtGui.QPrinter.HighResolution)

        printDialog = QtGui.QPrintDialog(printer, self)
        if printDialog.exec_() == QtGui.QDialog.Accepted:
            painter = QtGui.QPainter(printer)
            rect = painter.viewport()
            size = self.image.size()
            size.scale(rect.size(), QtCore.Qt.KeepAspectRatio)
            painter.setViewport(rect.x(), rect.y(), size.width(), size.height())
            painter.setWindow(self.image.rect())
            painter.drawImage(0, 0, self.image)
            painter.end()

    def penColor(self):
        return self.myPenColor

    def penWidth(self):
        return self.myPenWidth

    def clearImage(self):
        """
        Function called by QPushButton "Clear View"
        """
        self.initObjet()
        self.image.fill(QtGui.qRgb(255, 255, 255))
        self.listview.clear()
        self.listspeed.clear()
        self.drawLineFront()
        self.printLastPosition()
        self.update()

    def setPenColor(self, newColor):
        """
        Function called by selector color in ActionBar (when you change color)
        Set the currentColor and currentList for draw
        """
        self.myPenColor = newColor
        ptr = self.myPenColor.name().__str__()
        index = 0
        while ptr != self.colorHex[index]:
            index += 1
        self.currentColor = self.colorName[index]
        self.currentList = self.objet.returnObject(self.currentColor).point

    def setPenWidth(self, newWidth):
        self.myPenWidth = newWidth

    def itemChanged(self, curr, prev):
        """
        This function is called by one signal Qt when speed (of QListView)
        is editing

        :param curr: curent item changed
        :type curr: QtCore.QModelIndex
        :param prev: previous item
        :type prev: QtCore.QModelIndex
        """
        self.listspeed.editItem(curr)

    def saveXml(self):
        """
        Return Save object instantiated
        """
        return (Save(self.objet))

    def readXml(self, path):
        """
        Call Save object and read a xml file
        This function is called by Ctrl+O or File->Open (Cf: MainWindow)
        Draw on image data read

        :param path: path read file return by chooseFile function (mainWindow)
        :type path: string
        """
        self.clearImage()
        tmp = Save(self.objet)
        tmp.readXmlToScene(path)
        self.drawFromFile()

    def drawFromFile(self):
        """
        Draw the drawing on image from data read
        Add information in item and speed (QtGui.QListView)
        """
        the_list = self.objet.returnListObjet()
        for item in the_list:
            self.currentColor = item.color
            self.currentList = item.point
            self.myPenColor = QtGui.QColor(item.color)
            if (item.point.nbPointPixel > 0):
                self.listview.addItem('Thread %s' % item.color)
                self.listspeed.addItem('Thread %s' % item.color)
            a = 0
            point = item.point.list_point_pixel
            print item.point
            print point
            while a < len(item.point.list_point_pixel):
                self.listview.addItem('\titem %s' % str(a+1))
                self.listspeed.addItem(item.point.list_speed[a])
                if (a >= 1):
                    self.drawSegment(point[a-1], point[a])
                a += 1

    def threadRoll(self):
        """
        Create Thread Object corresponding to how many
        sphero are connected
        """
        threads = []
        mutex = []

        list_sphero = self.objet.returnListObjet()
        for item in list_sphero:
            if (item.connect == True):
                print "Name sphero avant le toThread "
                print item.name
                print item.addr
                print item.socket
                print "\end avant toThread /"
                t = toThread(item)
                t.start()
                threads.append(t)

        for t in threads:
            t.join()

    def printLastPosition(self):
        """
        Print on image last position return by gyroscope
        """
        the_list = self.objet.returnListObjet()
        for item in the_list:
            if (item.point.last_gyro_meter != None):
                #print item.point.last_gyro_meter
                self.drawPoint(item.point.last_gyro_meter, item.color)
            else:
                self.drawPoint(QtCore.QRectF(0, 800, 4, 4), item.color)

    def led(self):
        """
        Create Led object (window) and execute this new window (QDialog)
        """
        tmp = Led(self.objet.returnListObjet())

        tmp.show()
        tmp.exec_()

    def setGyro(self):
        tmp = Gyro(self.objet.returnListObjet())

        tmp.show()
        tmp.exec_()

    def initObjet(self):
        """
        Initialize empty Point object for Sphero object
        Get the current list from current color
        """
        list_obj = self.objet.returnListObjet()
        last = None
        for item in list_obj:
            tmp = Point(self.widthMeter, self.heigthMeter, self.width, self.heigth)
            if (item.point != None):
                last = item.point.last_gyro_meter
            item.setPoint(tmp)
            item.point.last_gyro_meter = last
        self.currentList = self.objet.returnObject(self.currentColor).point

    def setObjet(self, obj):
        """
        Set infos for Sphero object when device is connected

        :param obj: tuple return by Connect object contains name, addr, and socket
        :type obj: tuple(string, string, string, SpheroAPI, bool)
        """
        for tmp in obj:
            if (tmp[0] == "Sphero-RBR"):
                the_object = self.objet.returnObject("black")
                the_object.setInfos(tmp)
            elif (tmp[0] == "Sphero-GRY"):
                the_object = self.objet.returnObject("blue")
                the_object.setInfos(tmp)
            elif (tmp[0] == "Sphero-AAA"):
                the_object = self.objet.returnObject("green")
                the_object.setInfos(tmp)
            elif (tmp[0] == "Sphero-BYO"):
                the_object = self.objet.returnObject("cyan")
                the_object.setInfos(tmp)
            elif (tmp[0] == "Sphero-GGB"):
                the_object = self.objet.returnObject("gray")
                the_object.setInfos(tmp)
            elif (tmp[0] == "Sphero-WPR"):
                the_object = self.objet.returnObject("darkRed")
                the_object.setInfos(tmp)
            elif (tmp[0] == "Sphero-BOY"):
                the_object = self.objet.returnObject("red") #petite sphero blanche
                the_object.setInfos(tmp)
            else:
                pass

    def drawLineFront(self):
        """
        Draw Grid in background
	# line in the background #Jeff
        """
        myPen = QtGui.QColor(QtCore.Qt.black)
        painter = QtGui.QPainter(self.image)
        painter.setPen(QtGui.QPen(myPen, 1,
                                  QtCore.Qt.SolidLine, QtCore.Qt.RoundCap, QtCore.Qt.RoundJoin))

        inter = self.width / 16
        tmp = inter
        while (tmp < self.width):
            point1 = QtCore.QPointF(tmp, 0)
            point2 = QtCore.QPointF(tmp, self.heigth)
            painter.drawLine(point1, point2)
            self.update()
            tmp += inter
        inter2 = self.heigth / 16
        tmp = inter2
        while (tmp < self.heigth):
            point1 = QtCore.QPointF(0, tmp)
            point2 = QtCore.QPointF(self.width, tmp)
            painter.drawLine(point1, point2)
            self.update()
            tmp += inter2

        point1 = QtCore.QPointF(0, 0)
        point2 = QtCore.QPointF(0, self.width)
        painter.drawLine(point1, point2)
        self.update()


    def modify_scene(self):
        """
        Function called by "Scene" Button
        Set height and the width of the "real" scene
        """
        h, ok = QtGui.QInputDialog.getDouble(self, "Scene", "Height:", 0.0, -10000, 10000, 2)

        if ok:
            self.heigthMeter = h
            w, ok = QtGui.QInputDialog.getDouble(self, "Scene", "Width:", 0.0, -10000, 10000, 2)

            if ok:
                self.widthMeter = w

        list_obj = self.objet.returnListObjet()
        last = None
        for item in list_obj:
            item.point.setScene(self.heigthMeter, self.widthMeter)


    def sequence(self):
        """
        Load the file from QFile Widget and call the launcher
        """
        path = self.file_widget.get_file_path()
        self.clearImage()
        tmp = Save(self.objet)
        tmp.readXmlToScene(path)
        self.drawFromFile()
        self.threadRoll()


    def load_file(self):
        """
        """
        path = self.file_widget.get_file_path()
        directory = QtGui.QFileDialog.getExistingDirectory(self,
                    "Choose the directory to open", path,  QFileDialog.ShowDirsOnly | QFileDialog.DontResolveSymlinks)

        if directory != '':
            self.file_widget.set_file_path(directory)


    def set_infos_from_file(self):
        """
        """
        self.heigthMeter = self.file_cfg.getHeight()
        self.widthMeter = self.file_cfg.getWidth()

        the_list = self.objet.returnListObjet()
        for tmp in the_list:
            print tmp.color
            if (tmp.color == "black"):
                tmp.setGyro(self.file_cfg.gyro_rbr())
            elif (tmp.color == "blue"):
                tmp.setGyro(self.file_cfg.gyro_gry())
            elif (tmp.color == "green"):
                tmp.setGyro(self.file_cfg.gyro_wpr())
            elif (tmp.color == "cyan"):
                tmp.setGyro(self.file_cfg.gyro_byo())	
            elif (tmp.color == "gray"):
                tmp.setGyro(self.file_cfg.gyro_ggb())
            elif (tmp.color == "red"):
            	tmp.setGyro(self.file_cfg.gyro_boy())
            elif (tmp.color == "darkRed"):
                tmp.setGyro(self.file_cfg.gyro_wpr())
            else:
                pass


