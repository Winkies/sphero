#!/usr/bin/env python
# -*- coding:utf-8 -*-

"""
Second Windows to connect Sphero
"""

from PyQt4.QtGui import *
from PyQt4.QtCore import *
from streaming import *
from error import SpheroError, SpheroConnectionError, \
SpheroFatalError, SpheroRequestError
from constants import MotorMode
from core import SpheroAPI
import bluetooth
import time

class   Connect(QDialog):
    """
    This window allows to search bluetooth device,
    to set color and to connect each sphero
    """
    def __init__(self):
        """
        This class implements QDialog class.
        It allows to list the connectables spheros.
        The button "ok" allows to initialize the connection (socket)
        and turn off led's sphero.
        """
        QDialog.__init__(self)

        self.color = ["black"]
        self.connect = ["connect"]

        self.table = QTableWidget()
        self.tmp = []
        self.auto = []
        self.sphero = []

        self.initConnect()
        self.initLayout()

    def initConnect(self):
        """
        Init size and title of window
        """
        self.setGeometry(500, 500, 500, 500)
        self.setWindowTitle("Connect Windows")

    def initLayout(self):
        """
        Init Layout of window: set QTableView and QPushButton
        """
        layout = QVBoxLayout(self)

        self.table.setRowCount(1)
        self.table.setColumnCount(3)
        ok = QPushButton("OK")
        ok.clicked.connect(self.autoSave)
        layout.addWidget(self.table)
        layout.addWidget(ok)

    def keyPressEvent(self, event):
        """
        Function event allows to exit with escape key

        :param event: touch pressed
        :type event: QKeyEvent
        """
        key = event.key()

        if key == Qt.Key_Escape:
            self.close()
        else:
            super(Connect, self).keyPressEvent(event)

    def autoConnect(self):
        """
        Scan bluetooth device during 10 sec
        Set QTableView's data:
                - first column: name of sphero
                - second column: QLineEdit for color
                - third column: QLineEdit for connect/disconnect
        """
        nearby_devices = bluetooth.discover_devices(duration=8,
                                                    lookup_names=True,
                                                    lookup_class=False)
        for addr, name in nearby_devices:
            if name.find("Sphero") != -1:
                self.tmp.append((name, addr))

        i = 0
        while i  < len(self.tmp) - 1:
            self.table.insertRow(i)
            i += 1

        i = 0
        for it, at in self.tmp:
            tmp = QComboBox()
            tmp.addItems(self.color)
            tmp2 = QComboBox()
            tmp2.addItems(self.connect)

            if it.find("Sphero") != -1:
                self.table.setCellWidget(i, 0, QLineEdit(it))
                self.table.setCellWidget(i, 1, QLineEdit("black"))
                self.table.setCellWidget(i, 2, QLineEdit("connect"))
                i += 1

    def autoSave(self):
        """
        Create a list of tuple
        Tuple equal: (name sphero, mac adress sphero, color, connect)
        Tuple type: (string, string, string, bool)
        """
        i = 0
        for it, at in self.tmp:
            ptr = "black"
            tmp2 = self.table.cellWidget(i, 2)
            time.sleep(0.5)
            ptr2 = tmp2.displayText()

            s = SpheroAPI(bt_name=it, bt_addr=at)
            if ptr2 == "connect":
                self.co(s)
                self.auto.append((it, at, ptr, s, True))
            i += 1
        self.close()

    def getSave(self):
        """
        Return a list of tuple of connected devices
        """
        return self.auto

    def co(self, s, c="black"):
        """
        Init the connection with SpheroAPI object
        (let's see core.py for more information)

        :param socket: socket Bluetooth corresponding to one connection
        :type socket: SpheroAPI
        :param color: name of color
        (by default black in order to turn off sphero)
        :type color: string
        """
        t = QColor(c).getRgb()

        s.connect()
        ssc = SensorStreamingConfig()
        ssc.stream_imu_angle()
        ssc.num_packets = 0
        ssc.sample_rate = 1

        s.set_option_flags(motion_timeout=True)
        print "set timeout", s.set_motion_timeout(5000).success

        s.set_stabilization(True)
        s.set_back_led_output(255)
        s.set_heading(0)
        s.set_rgb(t[0], t[1], t[2], True)

        s.configure_locator(0, 0, yaw_tare=90, auto=False)
        s.set_data_streaming(ssc)


