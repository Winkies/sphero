#!/usr/bin/python
# -*_- coding:utf-8 -*-

from PyQt4 import QtGui, QtCore

class   Point(object):
    """
    This object store data : list of point in pixel, in meter, last position
    of gyroscope
    One instance is made to one device connected (one color)
    """
    def __init__(self, width=0, height=0, imageWidth=0, imageHeight=0):
        """
        Initialize Point object

        :param width: width of scene in meter
        :type width: float
        :param width: height of scene in meter
        :type width: float
        :param width: width of image in pixel
        :type width: integer
        :param width: height of image in pixel
        :type width: integer
        """
        self.widthMeter = width
        self.heightMeter = height
        self.imageWidth = imageWidth
        self.imageHeight = imageHeight

        self.list_point_pixel = []
        self.list_point_meter = []
        self.list_speed = []

        self.last_gyro = None
        self.last_gyro_meter = None

        self.nbPointMeter = 0
        self.nbPointPixel = 0

    def addPointInList(self, point_pixel):
        """
        Add one point in list of point in pixel
        Convert this point in meter
        Add the point meter in list point in meter
        Update length of lists

        :param point_pixel: point in pixel
        :type point_pixel: QtCore.QPointF
        """
        point_meter = self.convertPixelToMeter(point_pixel)
        self.list_point_meter.append(point_meter)
        self.list_point_pixel.append(point_pixel)
        self.nbPointMeter = len(self.list_point_meter)
        self.nbPointPixel = len(self.list_point_pixel)
        
    def addPointInMeterInList(self, heightMeter, widthMeter):
        """
        Add one point in list of point in meter       
        Update length of list

        :param point_meter: point in meter
        :type point_meter: QtCore.QPointF
        """
        point_meter = QtCore.QPointF(heightMeter, widthMeter)

        self.list_point_meter.append(point_meter)
        self.nbPointMeter = len(self.list_point_meter)

    def addPointInListFromFile(self, point_pixel):
        """
        Add one point in list of point in meter
        Convert this point in pixel
        Add the point pixel in list point in pixel
        Update length of lists

        :param point_meter: point in meter
        :type point_meter: QtCore.QPointF
        """
        point_meter = self.convertPixelToMeter(point_pixel)
        self.list_point_pixel.append(point_pixel)
        self.list_point_meter.append(point_meter)
        self.nbPointMeter = len(self.list_point_meter)
        self.nbPointPixel = len(self.list_point_pixel)

    def addBreakInList(self, point_break):
        """
        Add break just in list point meter (list used in Rolling function)

        :param point_break: x = -1 and y = -1
        :type point_break: QtCore.QPointF
        """
        self.list_point_meter.append(point_break)
        self.nbPointMeter = len(self.list_point_meter)

    def addSpeedInList(self, speed):
        """
        Add speed in list speed

        :param speed: speed
        :type speed: QWidgetItem
        """
        self.list_speed.append(speed)

    def convertPixelToMeter(self, point):
        """
        Convert one point in pixel to point in meter

        :param point: point in pixel
        :type point: QtCore.QPointF
        :return: point converted
        :rtype: QtCore.QPointF
        """
        x = (point.x() * self.widthMeter / self.imageWidth) * 100
        y = (self.heightMeter - point.y() * self.heightMeter / self.imageHeight) * 100
        return (QtCore.QPointF(x, y))

    def convertMeterToPixel(self, point):
        """
        Convert one point in meter to point in pixel

        :param point: point in meter
        :type point: QtCore.QPointF
        :return: point converted
        :rtype: QtCore.QPointF
        """
        x = (point.x() * self.imageWidth / self.widthMeter) / 100
        y = -(((point.y() * self.imageHeight / self.heightMeter) / 100) - self.imageHeight)
        return (QtCore.QPointF(x, y))

    def setScene(self, height, width):
        """
        Setter function for the size scene

        :param height: height of the scene
        :type height: float
        :param width: width of the scene
        :type width: float
        """
        self.widthMeter = width
        self.heightMeter = height

    """
    Starting to implements default speed for each device connected
    Need to define slow, default, fast speed
    And modify theses functions
    """
    def modifySpeed(self, index, new_speed):
         self.list_speed[index] = QListWidgetItem(new_speed)

    def changeSpeed(self, name):
        i = 0
        while (i < len(list_speed)):
            val = list_speed[i].text()
            if (val != ""):
                if (val == "40"):
                    if (name == "Sphero-GRY"):
                        self.modifySpeed(i, "40")
                    elif (name == "Sphero-GRY"):
                        self.modifySpeed(i, "60")
                elif (val == "60"):
                    if (name == "Sphero-GRY"):
                        self.modifySpeed(i, "60")
            i += 1


