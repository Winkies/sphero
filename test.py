#!/usr/bin/python
# coding: utf-8

from streaming import *
from error import SpheroError, SpheroConnectionError, \
SpheroFatalError, SpheroRequestError
from constants import MotorMode
from core import *
import bluetooth
import time

if __name__ == '__main__':
    s2 = SpheroAPI(bt_name="Sphero-RBR", bt_addr="68:86:e7:04:11:1c")
    #s2 = SpheroAPI(bt_name="Sphero-YGY", bt_addr="68:86:e7:03:22:95")
    #s2 = SpheroAPI(bt_name="Sphero-GRY", bt_addr="68:86:e7:06:4e:55")
    #s2 = SpheroAPI(bt_name="Sphero-GBW", bt_addr="68:86:e7:03:d7:dc")

    s2.connect()
    ssc = SensorStreamingConfig()
    ssc.stream_imu_angle()
    ssc.num_packets = 0
    ssc.sample_rate = 1

    s2.set_option_flags(motion_timeout=True)
    print "set timeout s2", s2.set_motion_timeout(5000).success

    s2.set_stabilization(True)
    s2.set_back_led_output(255)
    s2.set_heading(0)

    s2.configure_locator(0, 0, yaw_tare=90, auto=False)
    s2.set_data_streaming(ssc)

    a = 0
    while a < 10 :
        print s2.read_locator()
        time.sleep(1)
        a += 1




