#!/usr/bin/env python
# -*- coding:utf-8 -*-

import threading
import time
import OSC
from core import *

class   OSCGetSpheroDataThread(threading.Thread):
    """
    This class implements thread python in order to get
    data from each sphero at the same time
    """
    def __init__(self, sphero):
        """
        Initialize Thread class
        Initialize Roll class

        :param point_objet: this object contains list speeds, list points, ..
        :type point_objet: list of Objet object
        """
        threading.Thread.__init__(self)
        self.sphero = sphero
        self.OSCClient = OSC.OSCClient()

    def run(self):
        """
        This function is called by start thread instruction.
        
        """
        print "Starting"
        msg = OSC.OSCMessage()

        while(1):
		# change la frequence de raffraichissment avec le time sleep
                time.sleep(.1)
                if (self.sphero.connect == True):
                        data = self.sphero.socket.read_locator()
			#databat = self.sphero.socket.get_power_state()
			# type(a)
			# adresse=("/",self.sphero.color)
                        msg.setAddress("/" + self.sphero.color)
			
			#print("x_pos")
			#print([data.x_pos , data.y_pos])
			
			msg.append([data.x_pos , data.y_pos , data.x_vel , data.y_vel , data.sog]) 
			#msg.append([data.x_pos,data.y_pos,data.x_vel,data.y_vel,data.sog,databat.batteryVoltage ,databat.batteryState])
			#Jeff avec bat
                        # "PowerState": high-level state of the power system
                        # or "BattVoltage"

			data = "Test"
			print(msg)
			#print(self.sphero.color)
                        # msg.setAddress("/yellow")
                        #msg.append(databat)
                        

			self.OSCClient.sendto(msg, ('192.168.0.111', 9001))  #Jeff 
			msg.clearData()
     
        
        
