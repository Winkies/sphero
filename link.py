#!/usr/bin/env python
# -*- coding:utf-8 -*-

from PyQt4.QtGui import *
from PyQt4.QtCore import *

class   Link(QDialog):
    def __init__(self):
        QDialog.__init__(self)

    self.color = ["black", "blue", "green", "cyan", "gray", "yellow" , "magenta" , "darkRed" , "red"]
    self.table = QTableWidget()

    self.combo_color = QComboBox()
    self.init_ComboBoxColor()

    self.link = {}

    self.initLink()
    self.initLayout()

    def initLink(self):
        self.setGeometry(500, 500, 500, 500)
        self.setWindowTitle("Link Windows")

    def initLayout(self):
        layout = QVBoxLayout(self)

        self.table.setRowCount(1)
        self.table.setColumnCount(2)
        ok = QPushButton("OK")
        ok.clicked.connect()
        layout.addWidget(ok)

    def keyPressEvent(self, event):
        """
        Function event allows to exit with escape key
        """
        key = event.key()

        if (key == Qt.Key_Escape):
            self.close()
        else:
            super(Link, self).keyPressEvent(event)

    def init_ComboBoxColor(self):
        for item in self.color:
            self.combo_color.addItems(item)

    def linked_in(self, connected_device):
        i = 0
        while i < len(connected_device) - 1:
            self.table.insertRow(i)
            i += 1

        i = 0
        for item in connected_device:
            self.table.etCellWidget(i, 0, QLineEdit(item))
            self.table.setCellWidget(i, 1, self.combo_color)
            i += 1

    def save_link(self):
        value = self.table.rowCount()
        it = 0
        i = 0

        dic = {}
        while it < value:
            tmp = self.table.cellWidget(it, 1).currentText()
            tmp2 = self.table.cellWidget(it, 2).currentText()
            dic[tmp] = tmp2
            it += 1

    def set_link(self, link):
        self.link = link

