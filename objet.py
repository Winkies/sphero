#!/usr/bin/env python
# -*- coding:utf-8 -*-

class   Sphero(object):
    """
    This class stores information about sphero found with bluetooth
    """
    def __init__(self, color, obj=None):
        """
        Initialize Sphero class from param color and objet

        :param color: color associated to this objet
        :type color: string
        :param obj: obj contains name, addr and socket of device connected
        tuple(name, addr, color, socket, connect)
        :type obj: tuple(string, string, string, SpheroAPI, bool)
        """
        self.name = ""
        self.addr = ""
        self.socket = None
        self.color = color
        self.connect = False

        self.point = None
        self.param_gyro = 1.6

    def setInfos(self, obj):
        """
        Set information relating from device connected

        :param obj: tuple(name, addr, color, socket, connect)
        :type obj: tuple(string, string, string, SpheroAPI, bool)
        """
        self.name = obj[0]
        self.addr = obj[1]
        self.socket = obj[3]
        self.connect = obj[4]

    def setPoint(self, point):
        """
        Set self.point variable of this class

        :param point: object contains informations like coordinates, speeds, ...
        :type point: Point object
        """
        self.point = point

    def setGyro(self, gyro):
        """
        Set self.param_gyro variable of this class

        :param gyro: new value of the gyroscope
        :type gyro: double
        """
        self.param_gyro = gyro

    def getColor(self):
        """
        Get color of this object

        :rtype: string
        """
        return (self.color)

class   Objet(object):
    """
    This class manages SpheroAPI object
    """
    def __init__(self, obj=None):
        """
        Initialize Objet object
        If obj equals None, initialize empty list of device connected

        :param obj: tuple of device connected
        :type obj: tuple(string, string, string, SpheroAPI, bool)
        """
        self.list_objet = []

        self.obj = obj

        self.createList()
        self.sortList()

    def createList(self):
        """
        Set color of each one sphero connected from their name and
        create list of Sphero object
        If no one device is connected, it instanciates empty Sphero object

        black: Sphero-RBR
        blue: Sphero-GRY
        green: Sphero-AAA
	cyan: Sphero-BYO
	gray: Sphero-GGB
        yellow: Sphero-GYW
	red: Sphero-BOY
	darkRed: Sphero-WPR
        """
        if (self.obj):
            for tmp in self.obj:
                if (tmp[0] == "Sphero-RBR"):
                    obj = Sphero("black", tmp)
                elif (tmp[0] == "Sphero-GRY"):
                    obj = Sphero("blue", tmp)
                elif (tmp[0] == "Sphero-AAA"):
                    obj = Sphero("green", tmp)
                elif (tmp[0] == "Sphero-BYO"):
                    obj.setColor("cyan")
                elif (tmp[0] == "Sphero-GGB"):
                    obj.setColor("gray")
               	elif (tmp[0] == "Sphero-BOY"):
                    obj.setColor("red")
               	elif (tmp[0] == "Sphero-WPR"):
                    obj.setColor("darkRed")
                else:
                    pass

                self.list_objet.append(obj)
        else:
            i = 0
            while (i < 9): 
                if (i == 0):
                    obj = Sphero("black")
                elif (i == 1):
                    obj = Sphero("blue")
                elif (i == 2):
                    obj = Sphero("green")
                elif (i == 3):
                    obj = Sphero("magenta")
                elif (i == 4):
                    obj = Sphero("cyan")
                elif (i == 5):
                    obj = Sphero("gray")
                elif (i == 6):
                    obj = Sphero("red")
 		elif (i == 7):
                    obj = Sphero("darkRed")
                self.list_objet.append(obj)
                i += 1

    def sortList(self):
        """
        Sort list of sphero connected from descending name
        """
        if (self.obj):
            self.list_objet.sort(key=lambda x: x.name, reverse=True)

    def printList(self):
        """
        Print must be print information contents in "Sphero" object list
        """
        for tmp in self.list_objet:
            print("color of element %s" % tmp.color)

    def returnObject(self, color):
        """
        Return Sphero object according to color

        :param color: name color
        :type color: string
        :return: object contians name, addr, socket, Point object
        :rtype: Sphero object
        """
        for tmp in self.list_objet:
            if (tmp.color == color):
                return(tmp)
            else:
                pass
        return(None)

    def autoConnect(self):
        """
        /!\ Wrong function : need to be redone
        """
        for tmp in self.obj:
            if (tmp[3].getReceiverCrashed):
                tmp[3].disconnect()
                del tmp[3]
                tmp[3] = SpheroAPI(tmp[0], tmp[1])
                tmp[3].connect()
            else:
                pass

    def returnListObjet(self):
        return(self.list_objet)
