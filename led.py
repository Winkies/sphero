#!/usr/bin/python
# -*- coding:utf-8 -*-

"""
Third Windows to set Led's Sphero
"""

from PyQt4.QtGui import *
from PyQt4.QtCore import *
import streaming
from error import SpheroError, SpheroConnectionError, SpheroFatalError, SpheroRequestError
from constants import MotorMode
import bluetooth
import time
from core import SpheroAPI

class   Led(QDialog):
    """
    This window allows to set direction back led of sphero's
    """
    def __init__(self, obj):
        QDialog.__init__(self)
	#nombre de sphero possible 6 ou 8
        self.val = 9

        self.obj = obj
        self.currentLed = None
        self.combo = QComboBox()
        self.tmp = []

        self.initCombo()
        self.initLed()
        self.initLayout()

    def initLed(self):
        """
        Init size and title of window
        """
        self.setGeometry(500, 500, 500, 500)
        self.setWindowTitle("Led Windows")

    def initLayout(self):
        """
        Init Layout of window: QComboBox, QPushButton
        """
        layout = QVBoxLayout(self)

        self.combo.addItems(self.tmp)
        self.combo.activated[str].connect(self.onActivated)
        ok = QPushButton("OK")
        ok.clicked.connect(self.ok)
        grande = QPushButton("Grand angle")
        grande.clicked.connect(self.grande)
        middle = QPushButton("Middle angle")
        middle.clicked.connect(self.middle)
        small = QPushButton("Small angle")
        small.clicked.connect(self.small)
        left = QPushButton("Tourne dans le sens Horaire /Left")
        left.clicked.connect(self.leftLed)
        right = QPushButton("Tourne dans le sens AntiHoraire /Right")
        right.clicked.connect(self.rightLed)
        roll = QPushButton("Test")
        roll.clicked.connect(self.rollLed)
        layout.addWidget(self.combo)
        layout.addWidget(grande)
        layout.addWidget(middle)
        layout.addWidget(small)
        layout.addWidget(right)
        layout.addWidget(left)
        layout.addWidget(roll)
        layout.addWidget(ok)

    def initCombo(self):
        """
        Set QComboBox with name of all sphero
        """
        a = 0
        for i in self.obj:
            if i.connect == True:
                if (a == 0):
                    self.currentLed = i.socket
                i.socket.set_back_led_output(255)
                self.tmp.append(i.name)
                a += 1

    def keyPressEvent(self, event):
        """
        Function event allows to exit with escape key
        """
        key = event.key()

        if (key == Qt.Key_Escape):
            self.close()
        else:
            super(Led, self).keyPressEvent(event)

    def onActivated(self, text):
        """
        Function event to QComboBox to set self.currentLed
        self.currentLed defined which sphero must be rotate
        """
        for i in self.obj:
            if i.name == text:
                self.currentLed = i.socket

    def leftLed(self):
        """
        Rotate sphero to 0 + self.val degrees to left
        """
        val = 0 + self.val
        self.currentLed.roll(0, val)
        time.sleep(1)
        self.currentLed.set_heading(0)
        time.sleep(1)
        self.currentLed.roll(0, 0)
        time.sleep(1)


    def rightLed(self):
        """
        Rotate sphero to 358 - self.val degrees to right
        """
        val = 358 - self.val
        self.currentLed.roll(0, val)
        time.sleep(1)
        self.currentLed.set_heading(0)
        time.sleep(1)
        self.currentLed.roll(0, 0)
        time.sleep(1)

    def rollLed(self):
        """
        Function allows to test direction with simple command roll
        when "test" button is clicked
        """
        self.currentLed.roll(60, 0)
        time.sleep(2)
        self.currentLed.roll(0, 0)
        time.sleep(1)

    def ok(self):
        """
        Close window and back Led when "ok" button is clicked
	for tmp in self.auto: #Jeff
   	tmp[3].configure_locator(0, 0, yaw_tare=90, auto=False); #JEff
        """
        for i in self.obj:
            if (i.connect == True):
                i.socket.set_back_led_output(0)
	#	I.socket.configure_locator(0, 0, yaw_tare=90, auto=False) #JEff 
        self.close()

    def grande(self):
        """
        Set the value of variable self.val to 20 (degrees)
        """
        self.val = 20

    def middle(self):
        """
        Set the value of variable self.val to 5 (degrees)
        """
        self.val = 5

    def small(self):
        """
        Set the value of variable self.val to 2 (degrees)
        """
        self.val = 2
